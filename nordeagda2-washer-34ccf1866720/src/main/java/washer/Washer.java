package washer;

import oracle.jrockit.jfr.JFR;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by KW on 9/11/17.
 */
public class Washer{


    private ExecutorService watek = Executors.newFixedThreadPool(3);


    private WasherProgram[] programs;
    private WasherProgram program = null;
    private Timer doorTimer = new Timer(5000, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            doorCheckBox.setSelected(true);
        }
    });

    public Washer() {

        doorTimer.setRepeats(false);

        programs = new WasherProgram[3];
        programs[0] = new WasherProgram(new String[]{"Pranie wstepne", "Suszenie", "Wirowanie"});
        programs[1] = new WasherProgram(new String[]{"Suszenie", "Wirowanie"});
        programs[2] = new WasherProgram(new String[]{"Wirowanie 600", "Wirowanie 700", "Wirowanie 800", "Wirowanie 900", "Wirowanie 1000"});


        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Kliknięto guzik.");

                    if (program1RadioButton.isSelected()) {
                        program = programs[0];
                    } else if (program2RadioButton.isSelected()) {
                        program = programs[1];
                    } else if (program3RadioButton.isSelected()) {
                        program = programs[2];
                    }

                    doorCheckBox.setSelected(false);
                    watek.submit(program);

            }
        });
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    private JRadioButton program1RadioButton;
    private JRadioButton program2RadioButton;
    private JRadioButton program3RadioButton;
    private JButton startButton;
    private JCheckBox wodaWPralceCheckBox;
    private JCheckBox doorCheckBox;
    private JPanel mainPanel;
    private JLabel statusLabel;
    private JProgressBar progressBar;

    private class WasherProgram implements Runnable {
        private String[] phases = null;
        private boolean shouldBreak = false;

        public WasherProgram(String[] phases) {
            this.phases = phases;
        }

        @Override
        public void run() {
            program1RadioButton.setEnabled(false);
            program2RadioButton.setEnabled(false);
            program3RadioButton.setEnabled(false);

            shouldBreak = false;
            startButton.setText("Stop");
            progressBar.setValue(0);
            double progress = ((double) 100.0 / (double) phases.length);
            double progress_jump = progress / 10.0;
            for (int i = 0; i < phases.length; i++) {

                statusLabel.setText(phases[i]);

                try {
                    for (int j = 0; j < 10; j++) {
                        Thread.sleep(100);
                        double val = ((progress * i) + (j * progress_jump));
                        progressBar.setValue((int)val);
                    }
                } catch (InterruptedException ie) {
                    System.out.println(ie);
                }
                if (shouldBreak) {
                    break;
                }
            }
            progressBar.setValue(100);

            statusLabel.setText("Koniec");
            startButton.setText("Start");
            program1RadioButton.setEnabled(true);
            program2RadioButton.setEnabled(true);
            program3RadioButton.setEnabled(true);

            doorTimer.start();
        }

        public void stopProgram() {
            statusLabel.setText("Przerywam pranie...");
            shouldBreak = true;
        }

        public String[] getPhases() {
            return phases;
        }
    }
}
